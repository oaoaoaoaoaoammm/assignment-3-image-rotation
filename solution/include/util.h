//
// Created by danya on 12.12.2022.
//

#ifndef IMAGE_TRANSFORMER_UTIL_H
#define IMAGE_TRANSFORMER_UTIL_H

#include <stdio.h>

FILE * file_open(char const * file_name, char const * per);

void file_close(FILE * file_name);

#endif //IMAGE_TRANSFORMER_UTIL_H
