//
// Created by danya on 12.12.2022.
//

#ifndef IMAGE_TRANSFORMER_INSTRUMENT_H
#define IMAGE_TRANSFORMER_INSTRUMENT_H

#include "image.h"

long get_padding(struct image const * img);

void img_clear(struct image * image);

void allocate_image(size_t width, size_t height, struct image * img);

#endif //IMAGE_TRANSFORMER_INSTRUMENT_H
