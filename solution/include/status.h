//
// Created by danya on 12.12.2022.
//

#ifndef IMAGE_TRANSFORMER_STATUS_H
#define IMAGE_TRANSFORMER_STATUS_H

enum status_read {
    READ_OK = 0,
    READ_HEADER_ERROR,
    READ_PIXELS_ERROR,
};

enum status_write {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_PIXELS_ERROR,
};


#endif //IMAGE_TRANSFORMER_STATUS_H
