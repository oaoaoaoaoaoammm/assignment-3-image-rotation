//
// Created by danya on 12.12.2022.
//

#ifndef IMAGE_TRANSFORMER_FUNCTIONS_IMAGE_H
#define IMAGE_TRANSFORMER_FUNCTIONS_IMAGE_H

#include <stdio.h>

#include "image.h"

enum status_read read_image(FILE * file, struct image * img);

enum status_write write_image(FILE * file_out, struct image const * img);

#endif //IMAGE_TRANSFORMER_FUNCTIONS_IMAGE_H
