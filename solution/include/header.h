//
// Created by danya on 12.12.2022.
//

#ifndef IMAGE_TRANSFORMER_HEADER_H
#define IMAGE_TRANSFORMER_HEADER_H

#define BfType 19778
#define BfReserved 0
#define BiSize 40
#define BiPlanes 1
#define BiBitCount 24
#define BiCompression 0
#define BiXPelsPerMeter 0
#define BiYPelsPerMeter 0
#define BiClrUsed 0
#define BiClrImportant 0


struct __attribute__((packed)) header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

#endif //IMAGE_TRANSFORMER_HEADER_H
