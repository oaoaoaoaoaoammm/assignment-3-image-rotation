//
// Created by danya on 12.12.2022.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <malloc.h>
#include <stdint.h>
#include <stdio.h>

#include "status.h"


struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image {
    size_t width, height;
    struct pixel* data;
};


#endif //IMAGE_TRANSFORMER_IMAGE_H
