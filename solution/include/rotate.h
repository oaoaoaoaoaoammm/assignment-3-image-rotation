//
// Created by danya on 12.12.2022.
//

#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include "image.h"

void rotate(struct image const * old_img, struct image * new_img);

#endif //IMAGE_TRANSFORMER_ROTATE_H
