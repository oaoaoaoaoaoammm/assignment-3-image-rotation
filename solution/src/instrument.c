//
// Created by danya on 12.12.2022.
//

#include "image.h"

long get_padding(struct image const * const img) {
    return (long)(img->width % 4);
}

void img_clear(struct image * const image) {
    free(image->data);
}

void allocate_image(size_t const width, size_t const height, struct image * const img) {
    *img = (struct image) {
            .width = width,
            .height = height,
            .data = malloc(sizeof(struct pixel) * height * width)
    };
}

