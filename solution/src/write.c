//
// Created by danya on 12.12.2022.
//

#include "image.h"
#include "header.h"
#include "instrument.h"

struct header static create_header(struct image const * const image) {
    size_t size_of_image = (sizeof(struct pixel) * image->width + get_padding(image)) * image->height;
    struct header header = {
            .bfType = BfType,
            .bfileSize = sizeof(struct header) + size_of_image,
            .bfReserved = BfReserved,
            .bOffBits = sizeof(struct header),
            .biSize = BiSize,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = BiPlanes,
            .biBitCount = BiBitCount,
            .biCompression = BiCompression,
            .biSizeImage = size_of_image,
            .biXPelsPerMeter = BiXPelsPerMeter,
            .biYPelsPerMeter = BiYPelsPerMeter,
            .biClrUsed = BiClrUsed,
            .biClrImportant = BiClrImportant
    };
    return header;
}

enum status_write static write_header(FILE * const file, struct image const * const image) {
    struct header new_header = create_header(image);
    size_t status_write_header = fwrite(&new_header, sizeof(struct header), 1, file);
    return status_write_header != 1 ? WRITE_HEADER_ERROR : WRITE_OK;
}

enum status_write static write_pixels(FILE * const file_out, struct image const * const img) {
    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, file_out) != img->width)
            return WRITE_PIXELS_ERROR;
        if (fseek(file_out, get_padding(img), SEEK_CUR) != 0) {
            return WRITE_PIXELS_ERROR;
        }
    }
    return WRITE_OK;
}

enum status_write write_image(FILE * const file_out, struct image const * const img) {
     if (write_header(file_out, img) != WRITE_OK) {
         return WRITE_HEADER_ERROR;
     }
     if (write_pixels(file_out, img) != WRITE_OK) {
         return WRITE_PIXELS_ERROR;
     }
    return WRITE_OK;
}
