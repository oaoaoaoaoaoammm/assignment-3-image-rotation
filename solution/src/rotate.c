//
// Created by danya on 12.12.2022.
//

#include "image.h"
#include "instrument.h"

void rotate(struct image const * const old_img, struct image * const new_img) {

    size_t new_width = old_img->height;
    size_t new_height = old_img->width;

    allocate_image(new_width, new_height, new_img);

    for (size_t i = 0; i < old_img->height; i++) {
        for (size_t j = 0; j < old_img->width; j++) {
            new_img->data[old_img->height * j + old_img->height - i - 1] = old_img->data[old_img->width * i + j];
        }
    }
}
