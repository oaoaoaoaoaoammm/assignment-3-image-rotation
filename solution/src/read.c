//
// Created by danya on 12.12.2022.
//

#include "image.h"
#include "header.h"
#include "instrument.h"

enum status_read static read_header(FILE * const file, struct header  * const header) {
    size_t statusRead = fread(header, sizeof(struct header), 1, file);
    return statusRead != 1 ? READ_HEADER_ERROR : READ_OK;
}

enum status_read static read_pixels(FILE * const input, struct image const * const image) {
    for (size_t i = 0; i < image->height; i++) {
        size_t count = fread(image->data + i * image->width, sizeof(struct pixel), image->width, input);
        if (count != image->width) {
            if(feof(input)) printf("Premature end of file.");
            return READ_PIXELS_ERROR;
        }
        if (fseek(input, get_padding(image), SEEK_CUR) != 0) {
            return READ_PIXELS_ERROR;
        }
    }
    return READ_OK;
}

enum status_read read_image(FILE * const file, struct image * const img) {
    struct header download_header;

    if (read_header(file, &download_header) != READ_OK) {
        return READ_HEADER_ERROR;
    }

    //printf("%d \n", download_header.biWidth);
    //printf("%d", download_header.biHeight);

    allocate_image(download_header.biWidth, download_header.biHeight, img);

    if (read_pixels(file, img) != READ_OK)  {
        return READ_PIXELS_ERROR;
    }

    return READ_OK;
}
