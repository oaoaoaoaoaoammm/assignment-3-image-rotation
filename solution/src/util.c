//
// Created by danya on 12.12.2022.
//

#include <stdio.h>

FILE * file_open(char const * const file_name, char const * const per) {
    FILE * file;
    file = fopen(file_name,per);
    return file;
}

void file_close(FILE * const file){
    fclose(file);
}
