#include "util.h"
#include "functions_image.h"
#include "instrument.h"
#include "rotate.h"

int main( int argc, char** argv ) {

      if (argc > 3) {
        printf("Error");
        return 1;
    }

    FILE * file_in = file_open(argv[1], "rb");
    FILE * file_out = file_open(argv[2], "wb");

    struct image download_image;
    enum status_read read_info = read_image(file_in, &download_image);
    if (read_info != READ_OK) {
        fprintf(stderr, "%u", read_info);
        return 1;
    }

    struct image save_image;
    rotate(&download_image, &save_image );

    enum status_write write_info = write_image(file_out, &save_image);
    if (write_info != WRITE_OK) {
        fprintf(stderr, "%u", write_info);
        return 1;
    }

    img_clear(&download_image);
    img_clear(&save_image);

    file_close(file_in);
    file_close(file_out);

    return 0;
}
